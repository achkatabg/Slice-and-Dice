var data = [60, 10, 15, 244, 333,3,66,48];

d3.select(".chart")
.selectAll("div")
.data(data)
.enter()
.append("div")
.style("width", function(d) { return d * 2 + "px"; })
.text(function(d) { return '$ ' + d; });