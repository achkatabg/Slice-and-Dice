google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Birthrate', 'Rise', 'Death'],
          ['2005', 4324, 545, 453],
          ['2006', 5453, 324, 654],
          ['2007', 3213, 432, 787],
          ['2008', 4000, 312, 677]
        ]);

        var options = {
          chart: {
            title: 'People growth',
          }
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }