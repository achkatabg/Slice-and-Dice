      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Sleep',     7],
          ['Eat',      2],
	  ['Rave',     1],
          ['Gaming',    3],
          ['Coding',   12],
          ['Repeat',    1]
          ]);

        var options = {
          title: 'DailyRoutine',
          is3D:true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }